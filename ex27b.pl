% Historia - nome da historia na Base de Dados
% Interp - como a historia foi entendida
demo(Historia,Interp):- story(Historia,Actos),
						infere(Actos,Interp).

infere(Actos,Interp):- encontra(Actos,NomeScript),
					   script(NomeScript,Interp),
					   unifica(Actos,Interp).

encontra(Historia,NomeScript)
:- member(act(_,_,Slots),Historia),
   member(Palavra,Slots),
   nonvar(Palavra),
   trigger(Palavra,NomeScript).

unifica([],_).
unifica([Ln|Rhistoria],[Ln|Rscript]):-unifica(Rhistoria,Rscript).
unifica(Historia,[_|Rscript]):- unifica(Historia,Rscript).

mostra(Historia):- demo(Historia,Interp),
				   escreve(Interp).

escreve([]).
escreve([X|R]):- write(X),nl,escreve(R).
