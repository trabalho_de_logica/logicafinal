:- [ex23b], % carregar o sistema de inferência
dynamic(voa/0),dynamic(tipo_motor/1),
dynamic(numero_motores/1),dynamic(velocidade/1).

% a) Base de Conhecimento:
agente(aviao,[voa]).
agente(planador,[tipo_motor(nenhum)]).
agente(motorizado,[tipo_motor(helice)]).
agente(monomotor,[numero_motores(1)]).
agente(bimotor,[numero_motores(2)]).
agente(jacto,[tipo_motor(turbina)]).
agente(supersonico(V),[velocidade(V)]).

isa(planador,aviao).
isa(motorizado,aviao).
isa(monomotor,motorizado).
isa(bimotor,motorizado).
isa(jacto,aviao).
isa(supersonico(_),jacto).
